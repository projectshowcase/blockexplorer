import React, { Component } from 'react';
import Button from 'antd/lib/button';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src="https://theme.zdassets.com/theme_assets/224702/f1769fc082175cd2e7ef495fc941b08f235d0a41.png" className="App-logo" alt="logo" />

          <Button type="primary">Block Explorer SPA App</Button>
        </header>
      </div>
    );
  }
}

export default App;
